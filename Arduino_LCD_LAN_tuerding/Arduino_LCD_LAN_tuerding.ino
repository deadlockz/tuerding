// Ethernet shield attached to pins 10(CS), 11, 12, 13 // = SPI
// pin4 for SD Card (ss)

#include "rdm630.h"
#include <Ethernet.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>

#define RELAISPIN 5
#define LDRCHANNEL 2

#define BAUDRATE         9600
#define INVALIDRFID      1234567890ul

LiquidCrystal lcd(A0, A1, 9, 8, 7, 6);
rdm630 rfid;

bool openDoor = false;
unsigned int tick = 0;
byte data[6];
byte old_data[6];
byte leng;
unsigned long rfidResult = INVALIDRFID;

String req;
String s;
EthernetClient client;

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 200);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

// Atmega328P has 1024 Byte EEPROM = 28 lines a 36byte possible
#define TABSIZE 20

namespace TabLine {
  byte rfidcode[4];     // unsigned long 
  byte text[31];        // name (30 letters + \0)
  byte modus[1];        // allow, deactive
}

void tab_Read (int id) {
  EEPROM.get( 0 + id*36, TabLine::rfidcode);
  EEPROM.get( 4 + id*36, TabLine::text);
  EEPROM.get(35 + id*36, TabLine::modus);
}

void tab_Store (int id) {
  EEPROM.put( 0 + id*36, TabLine::rfidcode);
  EEPROM.put( 4 + id*36, TabLine::text);
  EEPROM.put(35 + id*36, TabLine::modus);
}

void setup() {
  Serial.begin(BAUDRATE);
  
  pinMode(RELAISPIN, OUTPUT);
  digitalWrite(RELAISPIN, HIGH); // high is off!
  openDoor = false;
  
  // clear eeprom (?)
  //for (int tt=0; tt < TABSIZE; ++tt) tab_Store (tt);
  
  lcd.begin(16, 2);
  
  // Trying DHCP
  if (Ethernet.begin(mac) == 0) {
    // initialize the Ethernet device not using DHCP:
    Ethernet.begin(mac, ip);
  }
  ip = Ethernet.localIP();
  
  server.begin();
  lcd.print(F("Tuerding"));
  lcd.setCursor(0,1);
  lcd.print(Ethernet.localIP());
  delay(2000);
}


void loop() {

HoppHere:
  tick++;
  
  client = server.available();
  if (!client == false) {
    req = client.readStringUntil('\r');
    
    client.flush();
    String last = String(rfidResult);
    if (rfidResult == INVALIDRFID) last = "---";
    client.println(F("HTTP/1.1 200 OK"));

    // requst handling -----------------------------
    
    int sindex = req.indexOf("GET /id=", 0);
    if (sindex == 0) {
      int id = atoi(req.substring(sindex+8, req.indexOf('?', sindex+8)).c_str());
      sindex = req.indexOf("rfid=", sindex+8);
      if (sindex >= 8) {
        unsigned long rfidcode = (unsigned long) atol(req.substring(sindex+5, req.indexOf('&', sindex+5)).c_str());
        *((unsigned long *) &TabLine::rfidcode) = rfidcode;
        
        sindex = req.indexOf("text=", 8);
        if (sindex >= 8) {
          String text = req.substring(sindex+5, req.indexOf('&', sindex+5));
          text.replace("%C3%A4", "ae");
          text.replace("%C3%84", "Ae");
          text.replace("%C3%BC", "ue");
          text.replace("%C3%9C", "Ue");
          text.replace("%C3%B6", "oe");
          text.replace("%C3%96", "Oe");
          text.replace("%C3%9F", "/3");
          text.replace("+", " ");
          text.replace("%2B", "+");
          text.replace("%20", " ");
          text.replace("%2F", "/");
          text.replace("%3D", "=");
          text.replace("%3F", "?");
          text.replace("%21", "!");
          text.replace("%26", "&");
          text.replace("%3B", ";");
          text.replace("%3A", ":");
          text.replace("%2C", ",");
          if (text.length() > 30) text = text.substring(0, 31);
          text.toCharArray(((char *) &TabLine::text), 31);

          sindex = req.indexOf("modus=", 8);
          if (sindex >= 0) {
            TabLine::modus[0] = req.charAt(sindex+6);

            // every part is handled correct and stored. We can write to eeprom, now!
            tab_Store(id); 
          }
        }        
      }
    }
        
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));
    client.println();
    client.print(F("<!DOCTYPE html>\n<html><head><title>T&uuml;rding</title><meta charset='utf-8' /></head>"));
    client.print(F("<body bgcolor='white' text='black'><h1>T&uuml;rding</h1>"));
    client.print(F("<pre>\n"));
    int sensorReading = analogRead(LDRCHANNEL);
    client.print("last RFID: " + last + "\n");
    client.print(F("sensor : "));
    client.print(sensorReading);
    client.print(F("\n<a href='/'>refresh site</a>\n\n"));

    // read from eeprom
    for (int tt=0; tt < TABSIZE; ++tt) {
      tab_Read(tt);

      s = "<form method='get' action='/id=" + String(tt) + "'>";
      if (*( (char *)(&TabLine::modus) ) == 'a') {
        s += "<span style='background: #55dd55;'>";
      } else {
        s += "<span>";
      }
      if (tt < 9) {
        s += "Rule  " + String(tt+1) + ": </span>";
      } else {
        s += "Rule " + String(tt+1) + ": </span>";
      }
      s += "<input type='text' name='rfid' maxlength='10' size='10' value='" + String(*( (unsigned long *)(&TabLine::rfidcode) )) + "'> ";
      s += "<input type='text' name='text' maxlength='30' size='30' value='" + String( (char *)(&TabLine::text) ) + "'> ";
      s += "<select name='modus'><option value='" + String(*( (char *)(&TabLine::modus) )) + "' selected>";
      if (*( (char *)(&TabLine::modus) ) == 'a') {
        s += "allow</option>";        
      } else {
        s += "deny</option>";
      }
      client.print(s);
      client.print(F("<option value='a'>a = allow</option>"));
      client.print(F("<option value='d'>d = deny</option>"));
      client.print(F("</select> "));
      client.print(F("<input type='submit' value='save'>"));
      client.print(F("</form>"));
    }
    
    client.print(F("</pre>"));
    client.println(F("</body></html>"));
    client.stop();
  }

  delay(5);
  
  if( rfid.available() ) {    
    
    // Data valid
    rfid.getData(data, leng);
    
    if (
      old_data[0] == data[0] &&
      old_data[1] == data[1] &&
      old_data[2] == data[2] &&
      old_data[3] == data[3] &&
      old_data[4] == data[4] &&
      old_data[5] == data[5]      
    ) {
      // rfid is not new --------------
      if (openDoor) {
        tick=0; // start timer again for same rfid
      } else {
        if (rfidResult == INVALIDRFID) {
          // maybe a valid rfid try again?
          old_data[0] = ~old_data[0];
          // this should force a second read and check of the rfid
        }
      }
   
      goto HoppHere; // jump to beginning and tick
      
    } else {
      // store to compare -------------
      old_data[0] = data[0];
      old_data[1] = data[1];
      old_data[2] = data[2];
      old_data[3] = data[3];
      old_data[4] = data[4];
      old_data[5] = data[5];
      tick=0;
    }
    
    rfidResult = 
      ((unsigned long int)data[1]<<24) + 
      ((unsigned long int)data[2]<<16) + 
      ((unsigned long int)data[3]<<8) + data[4];
  }


  if (tick%200 == 0) {
    lcd.clear();
    lcd.setCursor(0, 0);
    if (rfidResult != INVALIDRFID) {
      lcd.print(String(rfidResult));
    } else {
      lcd.print(F("Tuerding"));
    }
    lcd.setCursor(0, 1);
    lcd.print(String((char *) TabLine::text));  
  }

  if (openDoor == false) {
    if(isAllowed() == true) {
      openDoor = true;
      digitalWrite(RELAISPIN, LOW); // LOW = Relais is on
      lcd.setCursor(0, 1);
      lcd.print(String((char *) TabLine::text)); 
      tick = 0;     
    } else {
      openDoor = false;
      digitalWrite(RELAISPIN, HIGH); // HIGH = Relais is off
    }
  }
  
  if (tick>600) {
    openDoor = false;
    rfidResult = INVALIDRFID;
    digitalWrite(RELAISPIN, HIGH); // HIGH = Relais is off  
    tick = 0;
  }
}


// ----------------------------------------------------------
bool isAllowed(void) {
  bool back = false;
  
  if (rfidResult != INVALIDRFID) {
    for (int tt=0; tt < TABSIZE; ++tt) {
      tab_Read(tt);
      if (*( (unsigned long *)(&TabLine::rfidcode) ) == rfidResult) {
        if (*( (char *)(&TabLine::modus) ) == 'a') {
          back = true;
          tt = TABSIZE;
        }
      }
    }
  }
  if (!back) {
    String text = "";
    text.toCharArray(((char *) &TabLine::text), 31);  
  }
  return back;
}
// ----------------------------------------------------------


