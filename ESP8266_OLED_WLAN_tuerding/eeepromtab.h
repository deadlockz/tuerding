#include "extEEPROM.h"

// 32 k bytes (32768 = 0x8000) = 256 kbits

// 1 eeprom on the bus, 64 byte pages, 0x50 = default I2C EEPROM address
extEEPROM myEEPROM(kbits_256, 1, 64, 0x50);

#define TABSIZE 25


namespace ConfigData {  
  byte ssid[60];        // I am a AP! these values must be set 
  byte pswd[40];        // to connect to me
}

namespace TabLine {
  byte rfidcode[4];     // unsigned long 
  byte text[31];        // name (30 letters + \0)
  byte modus[1];        // allow, deactive
}

void tab_Read (int id) {
  myEEPROM.read(100 + id*36, TabLine::rfidcode,  4);
  myEEPROM.read(104 + id*36, TabLine::text,     31);
  myEEPROM.read(135 + id*36, TabLine::modus,     1);
}

void tab_Store (int id) {
  myEEPROM.write(100 + id*36, (byte *) &TabLine::rfidcode,  4);
  myEEPROM.write(104 + id*36, (byte *) &TabLine::text,     31);
  myEEPROM.write(135 + id*36, (byte *) &TabLine::modus,     1);
}

void eee_Init () {
  delay(3000);
  int i2cStat = myEEPROM.begin(myEEPROM.twiClock100kHz);
  if ( i2cStat != 0 ) {
    Serial.println(F("I2C EEPROM Problem"));
    return;
  }
  
  // initial write/reset ---------------------------------------------------------------
  if (digitalRead(flashBtn) == LOW) {
    myEEPROM.write( 0, (byte *) "Tuerding",   60);
    myEEPROM.write(60, (byte *) "verySecret", 40);
    for (int tt=0; tt < TABSIZE; ++tt) tab_Store (tt);
  }
  // -----------------------------------------------------------------------------------

  myEEPROM.read( 0, ConfigData::ssid, 60);
  myEEPROM.read(60, ConfigData::pswd, 40);
}

