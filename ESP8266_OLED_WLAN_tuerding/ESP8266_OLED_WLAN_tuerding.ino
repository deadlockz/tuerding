#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include "rdm630.h"
#include "myPinout.h"
#include "SSD1306.h"
#include "font.h"
#include "eeepromtab.h"

#define BAUDRATE         9600
#define INVALIDRFID      1234567890ul

SSD1306 display(0x3c, D2, D1);
rdm630 rfid;

bool openDoor = false;
unsigned int tick = 0;
byte data[6];
byte old_data[6];
byte leng;
unsigned long rfidResult = INVALIDRFID;

WiFiServer server(80);
IPAddress apIP(192, 168, 1, 1);

void setup() {
  Serial.begin(BAUDRATE);
  
  pinMode(flashBtn, INPUT_PULLUP);
  pinMode(relais,   OUTPUT);
  pinMode(wifiLED,  OUTPUT);
  
  digitalWrite(wifiLED, LOW);   // HIGH = LED is off
  digitalWrite(relais,  HIGH);  // LOW = Relais is on // HIGH = Relais is off

  eee_Init();
  digitalWrite(wifiLED, HIGH);  // HIGH = LED is off

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP((char *) ConfigData::ssid, (char *) ConfigData::pswd);
  Serial.print("Wifi Password: ");
  Serial.println((char *) ConfigData::pswd);

  server.begin();
  
  display.init();
  display.setFont(DejaVu_Sans_Condensed_8);
}

void loop() {

HoppHere:
  tick++;
  
  WiFiClient client = server.available();
  if (!client == false) {
    String req = client.readStringUntil('\r');
    client.flush();
    String last = String(rfidResult);
    if (rfidResult == INVALIDRFID) last = "---";
    String s =  "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
    client.print(s);
    s = "<!DOCTYPE html>\n<html><head><title>T&uuml;rding</title><meta charset='utf-8' /></head>"
        "<body bgcolor='white' text='black'><h1>T&uuml;rding</h1><pre>"
        "Last RFID: " + last + "\n"
        "<a href='/'>refresh site</a>\n\n";
    client.print(s);

    // requst handling -----------------------------
    
    int sindex = req.indexOf("id=", 0);
    if (sindex >= 0) {
      int id = atoi(req.substring(sindex+3, req.indexOf('?', sindex+3)).c_str());
      
      sindex = req.indexOf("rfid=", sindex+5);
      if (sindex >= 0) {
        unsigned long rfidcode = (unsigned long) atol(req.substring(sindex+5, req.indexOf('&', sindex+5)).c_str());
        *((unsigned long *) &TabLine::rfidcode) = rfidcode;
        
        sindex = req.indexOf("text=", sindex+7);
        if (sindex >= 0) {
          String text = req.substring(sindex+5, req.indexOf('&', sindex+5));
          text.replace("%C3%A4", "ae");
          text.replace("%C3%84", "Ae");
          text.replace("%C3%BC", "ue");
          text.replace("%C3%9C", "Ue");
          text.replace("%C3%B6", "oe");
          text.replace("%C3%96", "Oe");
          text.replace("%C3%9F", "/3");
          text.replace("+", " ");
          text.replace("%2B", "+");
          text.replace("%20", " ");
          text.replace("%2F", "/");
          text.replace("%3D", "=");
          text.replace("%3F", "?");
          text.replace("%21", "!");
          text.replace("%26", "&");
          text.replace("%3B", ";");
          text.replace("%3A", ":");
          text.replace("%2C", ",");
          if (text.length() > 30) text = text.substring(0, 31);
          text.toCharArray(((char *) &TabLine::text), 31);

          sindex = req.indexOf("modus=", sindex+6);
          if (sindex >= 0) {
            TabLine::modus[0] = req.charAt(sindex+6);

            // every part is handled correct and stored. We can write to eeprom, now!
            tab_Store(id); 
          }
        }
      }
    } else {
      sindex = req.indexOf("ssidn=", 0);
      if (sindex > 0) {
        String text = req.substring(sindex+6, req.indexOf('&', sindex+6));
        text.replace("%C3%A4", "ae");
        text.replace("%C3%84", "Ae");
        text.replace("%C3%BC", "ue");
        text.replace("%C3%9C", "Ue");
        text.replace("%C3%B6", "oe");
        text.replace("%C3%96", "Oe");
        text.replace("%C3%9F", "/3");
        text.replace("+", " ");
        text.replace("%2B", "+");
        text.replace("%20", " ");
        text.replace("%2F", "/");
        text.replace("%3D", "=");
        text.replace("%3F", "?");
        text.replace("%21", "!");
        text.replace("%26", "&");
        text.replace("%3B", ";");
        text.replace("%3A", ":");
        text.replace("%2C", ",");
        if (text.length() > 59) text = text.substring(0, 60);
        text.toCharArray(((char *) &ConfigData::ssid), 60);
        
        sindex = req.indexOf("&pswd=", sindex+6);
        if (sindex > 0) {
          text = req.substring(sindex+6, req.indexOf('&', sindex+6));
          text.replace("%C3%A4", "ae");
          text.replace("%C3%84", "Ae");
          text.replace("%C3%BC", "ue");
          text.replace("%C3%9C", "Ue");
          text.replace("%C3%B6", "oe");
          text.replace("%C3%96", "Oe");
          text.replace("%C3%9F", "/3");
          text.replace("+", " ");
          text.replace("%2B", "+");
          text.replace("%20", " ");
          text.replace("%2F", "/");
          text.replace("%3D", "=");
          text.replace("%3F", "?");
          text.replace("%21", "!");
          text.replace("%26", "&");
          text.replace("%3B", ";");
          text.replace("%3A", ":");
          text.replace("%2C", ",");
          if (text.length() > 39) text = text.substring(0, 40);
          text.toCharArray(((char *) &ConfigData::pswd), 40);

          // every part is handled correct and stored. We can write to eeprom, now!
          myEEPROM.write( 0, (byte *) &ConfigData::ssid, 60);
          myEEPROM.write(60, (byte *) &ConfigData::pswd, 40);

          s = "\n<span style='color: red;'>"
              "New SSID and PASSWORD will be active after restart. "
              "Please remember the new PASSWORD: " +String((char *) &ConfigData::pswd) + "</span>\n\n";
          client.print(s);
        }
      }
    }

    // read from eeprom
    for (int tt=0; tt < TABSIZE; ++tt) {
      tab_Read(tt);

      s = "<form method='get' action='/id=" + String(tt) + "'>";
      if (*( (char *)(&TabLine::modus) ) == 'a') {
        s += "<span style='background: #55dd55;'>";
      } else {
        s += "<span>";
      }
      if (tt < 9) {
        s += "Rule  " + String(tt+1) + ": </span>";
      } else {
        s += "Rule " + String(tt+1) + ": </span>";
      }
      s += "<input type='text' name='rfid' maxlength='10' size='10' value='" + String(*( (unsigned long *)(&TabLine::rfidcode) )) + "'> ";
      s += "<input type='text' name='text' maxlength='30' size='30' value='" + String( (char *)(&TabLine::text) ) + "'> ";
      s += "<select name='modus'><option value='" + String(*( (char *)(&TabLine::modus) )) + "' selected>";
      if (*( (char *)(&TabLine::modus) ) == 'a') {
        s += "allow</option>";        
      } else {
        s += "deny</option>";
      }
      s += "<option value='a'>a = allow</option>";
      s += "<option value='d'>d = deny</option>";
      s += "</select> ";
      s += "<input type='submit' value='save'>";
      s += "</form>";
      client.print(s);
    }
    s = "</pre><hr>\n"
        "<h3>Set AP WiFi Hotspot</h3>"
        "<form method='get' action='/setWifi'><pre>"
        "New SSID     <input type='text' name='ssidn' maxlength='60' size='60'>\n"
        "New Password <input type='text' name='pswd' maxlength='40' size='40'>\n"
        "<input type='hidden' name='dummy' value='dummy'>"
        "<input type='submit' value='save AP config'>"
        "</pre></form>";

    
    s += "</body></html>";
    client.print(s);
    
    String text = "";
    text.toCharArray(((char *) &TabLine::text), 31);
    goto HoppHere;
  }
  
  delay(5);

  if( rfid.available() ) {    
    
    // Data valid
    rfid.getData(data, leng);
    
    if (
      old_data[0] == data[0] &&
      old_data[1] == data[1] &&
      old_data[2] == data[2] &&
      old_data[3] == data[3] &&
      old_data[4] == data[4] &&
      old_data[5] == data[5]      
    ) {
      // rfid is not new --------------
      if (openDoor) {
        tick=0; // start timer again for same rfid
      } else {
        if (rfidResult == INVALIDRFID) {
          // maybe a valid rfid try again?
          old_data[0] = ~old_data[0];
          // this should force a second read and check of the rfid
        }
      }
   
      goto HoppHere; // jump to beginning and tick
      
    } else {
      // store to compare -------------
      old_data[0] = data[0];
      old_data[1] = data[1];
      old_data[2] = data[2];
      old_data[3] = data[3];
      old_data[4] = data[4];
      old_data[5] = data[5];
      tick=0;
    }
        
    for (int i=0; i < leng; ++i) {
        Serial.print(data[i], HEX);
    }
    Serial.println();
    
    rfidResult = 
      ((unsigned long int)data[1]<<24) + 
      ((unsigned long int)data[2]<<16) + 
      ((unsigned long int)data[3]<<8) + data[4];
  }

  if (tick%200 == 0) {
    display.clear();
    if (rfidResult != INVALIDRFID) {
      display.setTextAlignment(TEXT_ALIGN_RIGHT);
      display.drawString(128, 0, String(rfidResult));
    }
  
    display.drawLine(0, 10, 128, 10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, String((char *) ConfigData::ssid));

    display.drawString(0, 13, String((char *) TabLine::text));
  
    display.display();
  }

  if (openDoor == false) {
    if(isAllowed() == true) {
      openDoor = true;
      digitalWrite(relais, LOW); // LOW = Relais is on
      display.drawString(0, 13, String((char *) TabLine::text));
      display.display();
      tick = 0;     
    } else {
      openDoor = false;
      digitalWrite(relais, HIGH); // HIGH = Relais is off
    }
  }
  
  if (tick>600) {
    openDoor = false;
    rfidResult = INVALIDRFID;
    digitalWrite(relais, HIGH); // HIGH = Relais is off  
    tick = 0;
  }
}

// ----------------------------------------------------------
bool isAllowed(void) {
  bool back = false;
  
  if (rfidResult != INVALIDRFID) {
    for (int tt=0; tt < TABSIZE; ++tt) {
      tab_Read(tt);
      if (*( (unsigned long *)(&TabLine::rfidcode) ) == rfidResult) {
        if (*( (char *)(&TabLine::modus) ) == 'a') {
          back = true;
          tt = TABSIZE;
        }
      }
    }
  }
  if (!back) {
    String text = "";
    text.toCharArray(((char *) &TabLine::text), 31);  
  }
  return back;
}
// ----------------------------------------------------------

