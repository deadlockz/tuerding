
static const uint8_t D0   = 16;
static const uint8_t D1   = 5;  // i2c SCL
static const uint8_t D2   = 4;  // i2c SDA
static const uint8_t D3   = 0;  // this with reset to GND: flash
static const uint8_t D4   = 2;  // wifi led
static const uint8_t D5   = 14; // pwm
static const uint8_t D6   = 12; // pwm
static const uint8_t D7   = 13;
static const uint8_t D8   = 15; // pwm
static const uint8_t D9   = 3;  // RX
static const uint8_t D10  = 1;  // TX
static const uint8_t D11  = 9;  // does only work without elf2image  --flash_mode dio
static const uint8_t D12  = 10; // does only work without elf2image  --flash_mode dio

// gpio's 6 - 11 brocken on ESP-201

int wifiLED  = D4;
int flashBtn = D3;

int relais   = D7;
int redLED   = D8; // nicht mehr verbunden
int greenLED = D5; // nicht mehr verbunden
int blueLED  = D6; // nicht mehr verbunden

