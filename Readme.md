# Türding

Basierend auf einer Grundidee von Ralf Heesen (viele Türen und RFID Scanner
mit einem Verwaltungs-Webserver im Firmen WLAN) ist diese vereinfachte
Version des *Türdings* entstanden. Diese Version ist viel leichter zu bedienen und
hat fast keinen Wartungs- oder Installationsaufwand.

## Funktionsweise

Das *Türding* kann mit einfachen RFID-Chips auf der Basis einer Zugangsliste
ein Relais schalten. Eine typische Anwendung ist ein Türdrücker. Man kann
z.B. *Türding* hinter einer Fensterscheibe stellen, so dass Leute ihren
RFID-Chip vor das Fenster halten können, um eine Türe zu öffnen.

Sollte der RFID-Chip von *Türding* nicht als berechtigt erkannt werden,
so kann erst wieder nach ein paar Sekunden ein erneutes lesen des Chips
erfolgen. 

### Display

Im Display wird die SSID des WiFi Hotspots von *Türding* angezeigt. Diese
SSID ermöglicht zusammen mit dem Passwort das Hinterlegen der
Zugangsberechtigungen.

Hinter der SSID wird die eingelesene RFID angezeigt. Sollte dort nichts
stehen, so wurde auch kein SSID-Chip vor *Türding* gehalten.

Unter dem Strich im Display wird der Name, der einem Berechtigen RFID-Chip
zugeordnet ist, angezeigt. Solange man den RFID-Chip vor *Türding* hält,
wird das Relais geschaltet. Sobald man den Chip entfernt, hat man ca. 3
Sekunden Zeit, bis das Relais wieder aus geht.
 
### Vorteile gegenüber teure Transponder oder Schlüssel

- kaum Kosten bei Verlust des "Schlüssels"
- je Situation müssen so keine richtigen Schlüssel benutzt werden
- das richtige Schloss einer Türe wird geschont 
- lästiges Türdrücker-Drücken entfällt, da mehr Personen einen Zugang haben
- Werde kreativ: den Transponder an ein Halsband eines Tieres machen
  und so ganz andere Geräte schalten? Warum nicht! Hauptsache, Mensch und
  Tier verletzen sich nicht!

### Nachteile gegenüber Schlüssel oder teuren Alarm- und Schließsystemen

- Das *Türding* ersetzt niemals ein richtiges Schloss oder eine Alarmanlage!
- Es können (noch) keine Schließzeiten verwaltet werden.
- Es können nicht mehrere Türen mit einem zentralen System verwaltet werden,
  auch wenn entsprechende Konzepte bereits ausgearbeitet sind.

## Einstellungen

Um einen RFID Chip einem Namen und einen Zugang zu ermöglichen, muss man
sich in den WiFi Hotspot von *Türding* einloggen. Man muss danach die
Webseite `http://192.168.1.1/` aufrufen.

Auf dieser Seite sind 25 Regeln hinterlegt. In der ersten Spalte ist
die RFID eingetragen, dann der Name und zuletzt `allow` (Zugang möglich)
oder `deny` (kein Zugang). Mit dem `save` Button wird die Zeile gespeichert.
Regeln, die einen Zugang erlauben, sind grün markiert.

Um leichter die RFID eines Chips auf der Webseite einzutragen, kann man den
Chip an *Türding* halten und dann auf der Webseite `refresh site` klicken.
Die eingelesene RFID kann dann (kurz) auf dem Display und auf der Webseite (oben)
abgelesen bzw. kopiert werden.

Über die Webseite kann auch der Name und dass Passwort des Wifi Hotspots
geändert werden. Nach dem Speichern muss man jedoch *Türding* Aus- und
wieder Einschalten oder den (rst) Button drücken, damit die Einstellungen
aktiv werden!

### Reset

Drückt man in den ersten 3 Sekunden (bis die Blau LED erlischt) den (io0) Button,
dann werden alle gespeicherten Werte auf leere Standard-Werte zurückgesetzt.
Die SSID des WiFi Hotspots ist dann `Tuerding` und das Passwort ist `verySecret`.

## Technik

Aktuell sind aus Performance-Gründen nur bis zu 25 Zugänge speicherbar.
Theoretisch sind über 800 möglich, sollen nur je Zugang ein Modus, ein
Name (30 Zeichen) und die RFID gespeichert werden.

Der Code ist ausserdem so aufgebaut, dass die Reihenfolge des Request-Strings
wie folgt ist: **/id=0?rfid=xxxxx&text=xxxxx&modus=a** oder **/ssid=gaga?pswd=xxxxx&**

Außerdem wird bei `modus` nur das erste Zeichen nach dem `=` ausgewertet.


### USB Programmiermodus

- den (rst) Button gedrückt halten
- den (io0) Button gedrückt halten
- erst (rst) loslassen und in weniger als 3 Sekunden (io0) loslassen
- Schalter von `work` nach `prog` stellen
- In Arduino IDE 1.8.1 `upload` machen

Das *Türding* kann mit ca 500mA und 5V Netzteil betrieben werden. Eine
USB Powerbank ist also auch möglich.


### USB Seriell Debug

Insbesondere wird mit 9600 BAUD auf der Seriellen Konsole (micro USB) beim Start
das Wifi Passwort ausgegeben!


### Code

Alles ist auf [gitlab.com/deadlockz/tuerding](https://gitlab.com/deadlockz/tuerding) zu finden (Fritzing-Dateien, Quellcode, Dokumentation).

Eine 2. Code Version ohne WLAN (stattdessen Ethernet Shield) mit 16x2 LCD Display und
Arduino Uno ist dort auch hinterlegt.


## PCB (fritzing)

Die Bauteile lassen sich bei `aliexpress.com` günstig bestellen und
müssen nur an der Platine festgelötet werden:

- ESP8266 12F (2 bis 5 EUR)
- vier kleine Widerstände 0.25 Watt mit je 10k Ohm (100 Stück für ca 1 EUR)
- ein 4 Channels Logic Level Converter 3.3V to 5V (10 Stück für ca 1 EUR)
- ein 24C256 I2C interface EEPROM (ca 1 EUR)
- ein CP2102 USB to UART TTL 5PIN Connector Module (ca 3 EUR)
- ein 125Khz RFID Reader Module RDM6300 (ca 3 EUR)
- zwei kleine Taster und ein Schalter oder Jumper (unter 3 EUR)
- ein I2C OLED 128X32 Display SSD1306 (ca 3 EUR)
- ein 5V One Channel Relay Module Low Level (ca 1,50 EUR)

Ohne Platine, ohne Micro-USB Stromversorgung, ohne Kabel, ohne Kiste
drum herum, ohne Bauzeit, ohne Lötzinn und ohne Buchsen von Relais
nach draußen (Kiste) führen ist man also mit max. 21 EUR an Material
dabei. Davon sind aber nahezu **50% Aliexpress Lieferkosten, wenn** man
alles einzeln bestellt.

![oben](images/oben.png)

![Bauteilpositionen](images/bauteile.png)

![Rückseite](images/unten.png)
